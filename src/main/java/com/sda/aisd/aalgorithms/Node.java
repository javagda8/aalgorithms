package com.sda.aisd.aalgorithms;

import java.util.ArrayList;
import java.util.List;

public class Node {
    private int id;
    private String name;

    private Double distansToStart;

    private List<Edge> neighbours = new ArrayList<Edge>();

    private String trasa;

    public Node(int id, String name) {
        this.id = id;
        this.distansToStart = Double.POSITIVE_INFINITY;
        this.name = name;
        this.trasa = "" + id;
    }

    public void addNeighbour(Edge e) {
        neighbours.add(e);
    }

    public void removeNeighbour(Edge e) {
        neighbours.remove(e);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Edge> getNeighbours() {
        return neighbours;
    }

    public void setNeighbours(List<Edge> neighbours) {
        this.neighbours = neighbours;
    }

    public Double getDistansToStart() {
        return distansToStart;
    }

    public void setDistansToStart(Double distansToStart) {
        this.distansToStart = distansToStart;
    }

    public String getTrasa() {
        return trasa;
    }

    public void setTrasa(String trasa) {
        this.trasa = trasa;
    }
}
