package com.sda.aisd.aalgorithms;

public class Main {
    public static void main(String[] args) {
        Graph g = new Graph();

        Node n9 = new Node(9, null);
        n9.addNeighbour(new Edge(4, 9, 3));

        Node n8 = new Node(8, null);
        n8.addNeighbour(new Edge(9, 8, 7));
        n8.addNeighbour(new Edge(2, 8, 2));

        Node n7 = new Node(7, null);
        n7.addNeighbour(new Edge(5, 7, 6));
        n7.addNeighbour(new Edge(1, 7, 3));
        n7.addNeighbour(new Edge(9, 7, 8));
        n7.addNeighbour(new Edge(8, 7, 1));

        Node n6 = new Node(6, null);
        n6.addNeighbour(new Edge(3, 6, 5));
        n6.addNeighbour(new Edge(2, 6, 3));
        n6.addNeighbour(new Edge(5, 6, 7));

        Node n5 = new Node(5, null);
        n5.addNeighbour(new Edge(3, 5, 6));
        n5.addNeighbour(new Edge(3, 5, 4));
        n5.addNeighbour(new Edge(2, 5, 3));
        n5.addNeighbour(new Edge(5, 5, 1));

        Node n4 = new Node(4, null);
        n4.addNeighbour(new Edge(3, 4, 5));
        n4.addNeighbour(new Edge(1, 4, 1));

        Node n3 = new Node(3, null);
        n3.addNeighbour(new Edge(4, 3, 9));
        n3.addNeighbour(new Edge(1, 3, 7));
        n3.addNeighbour(new Edge(2, 3, 6));
        n3.addNeighbour(new Edge(2, 3, 5));
        n3.addNeighbour(new Edge(8, 3, 1));
        n3.addNeighbour(new Edge(5, 3, 2));

        Node n2 = new Node(2, null);
        n2.addNeighbour(new Edge(5, 2, 3));
        n2.addNeighbour(new Edge(2, 2, 8));

        Node n1 = new Node(1, null);
        n1.addNeighbour(new Edge(1, 1, 4));
        n1.addNeighbour(new Edge(5, 1, 5));
        n1.addNeighbour(new Edge(8, 1, 3));
        n1.addNeighbour(new Edge(8, 1, 7));

        g.addNodes(n1, n2, n3, n4, n5, n6, n7, n8, n9);
        g.dijkstra(6, 1);


    }
}
