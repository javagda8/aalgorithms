package com.sda.aisd.aalgorithms;

public class Edge {
    private int dist;
    private int from;
    private int to;

    public Edge(int dist, int from, int to) {
        this.dist = dist;
        this.from = from;
        this.to = to;
    }

    public int getDist() {
        return dist;
    }

    public void setDist(int dist) {
        this.dist = dist;
    }

    public int getFrom() {
        return from;
    }

    public void setFrom(int from) {
        this.from = from;
    }

    public int getTo() {
        return to;
    }

    public void setTo(int to) {
        this.to = to;
    }
}
