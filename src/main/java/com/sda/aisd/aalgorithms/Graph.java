package com.sda.aisd.aalgorithms;

import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;
import java.util.PriorityQueue;

public class Graph {
    // kluczem jest indeks węzła, wartość to węzeł
    private Map<Integer, Node> nodes = new HashMap<>();

    /**
     * Dodanie wierzchołka do grafu.
     *
     * @param n - wierzchołek który chcemy dodać.
     */
    public void addNode(Node n) {
        // dodajemy do mapy, kluczem jest indeks wierzchołka
        nodes.put(n.getId(), n);
    }

    public void addNodes(Node... n) {
        for (Node node : n) {
            addNode(node);
        }
    }

    /**
     * Algorytm dijkstry dla wierzchołków początkowego i końcowego
     *
     * @param startNode - początkowy wierzchołek (id)
     * @param stopNode  - id końcowego wierzchołka
     */
    public void dijkstra(int startNode, int stopNode) {
        PriorityQueue<Node> queue = new PriorityQueue<>(new Comparator<Node>() {
            @Override
            public int compare(Node o1, Node o2) {
                if (o1.getDistansToStart() > o2.getDistansToStart()) {
                    return 1;
                } else if (o1.getDistansToStart() < o2.getDistansToStart()) {
                    return -1;
                }
                return 0;
            }
        });

        // pobieram wierzcholek o id 'startNode'
        Node wierzcholekPoczatkowy = nodes.get(startNode);

        // ustawiam mu dystans 0.0
        // dist[source] ← 0
        wierzcholekPoczatkowy.setDistansToStart(0.0);

        // dodaje wierzcholek startowy do kolejki priorytetowej
        // add v to Q
        queue.add(wierzcholekPoczatkowy);

        // dopóki kolejka nie jest pusta
        // while Q is not empty:
        while (!queue.isEmpty()) {
            // wykonaj
            Node wierzcholek = queue.poll();

            // sprawdzamy czy dotarlismy do wierzcholka 'stopNode'
            if (wierzcholek.getId() == stopNode) {
                // znalezlismy nasz wierzcholek koncowy, mozemy wyjsc z petli
                break;
            }

            Double ourDistance = wierzcholek.getDistansToStart();
            // dla każdej z krawędzi
            for (Edge neighbourEdge : wierzcholek.getNeighbours()) {

                // nowy dystans do sąsiada, który będzie porównany ze starym dystansem
                Double distanceToNeighbour = ourDistance + neighbourEdge.getDist();
                int neighbourId = neighbourEdge.getTo(); // wierzcholek do ktorego idziemy

                // pobranie wierzcholka sasiada z mapy
                Node neighbour = nodes.get(neighbourId);

                // porównanie dystansów
                if (neighbour.getDistansToStart() > distanceToNeighbour) {
                    // jesli jest krótszy dystans, to aktualizujemy
                    neighbour.setDistansToStart(distanceToNeighbour);

                    // update trasy
                    neighbour.setTrasa(wierzcholek.getTrasa() + " -> " + neighbour.getId());

                    // dodać do kolejki
                    queue.add(neighbour);
                }
            }
        }

        Node koncowy = nodes.get(stopNode);
        double distance = koncowy.getDistansToStart();
        System.out.println(distance);

        System.out.println(koncowy.getTrasa());
    }
}
